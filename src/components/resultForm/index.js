import React from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";

const ResultForm = ({ formulario }) => {

  const mostrarDatosForm = ()=>{
      console.log(formulario);
  }
  return <div>
        <Button variant="contained" color="primary" onClick={mostrarDatosForm}>
          Guardar datos
        </Button>
       </div>;
};

const mapStateToProps = state => {
  return {
    formulario: state.formulario
  };
};

export default connect(mapStateToProps)(ResultForm);
