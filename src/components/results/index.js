import React, { useEffect } from "react";
import Page from "./page";
import { connect } from "react-redux";
import Loading from "../loading";

const Results = ({ loading }) => {
  useEffect(() => {
    console.log(loading);
  });

  return <div>{loading ? <Loading /> : <Page />}</div>;
};

const mapStateToProps = state => {
  return {
    loading: state.loading
  };
};

export default connect(mapStateToProps)(Results);
