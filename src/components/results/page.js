import React, { Fragment } from "react";
import showLoading from "../../redux/actions/loading/showLoading";
import hideLoading from "../../redux/actions/loading/hideLoading";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import TextField from '@material-ui/core/TextField';
import getDatosForm from '../../redux/actions/formulario/getDatosForm';
import setDatosForm from '../../redux/actions/formulario/setDatosForm';
import { useHistory } from "react-router-dom";

const Page = ({ showLoading, hideLoading,setDatosForm,getDatosForm,formulario }) => {
  const history = new useHistory();
  const handleLoading = () => {
    showLoading();
  };

  const guardarDatos = () =>{
    setDatosForm({Nombre: 'oscar', Apellido: 'Roman'});
  }

  const getDatos = ()=>{
    console.log(formulario);
  }

  const goToPage = ()=>{
    history.push('resultsForm')
  }

  return (
    <Fragment>
      <form noValidate autoComplete="off">
        <TextField id="standard-basic" label="Nombre" />
        <TextField id="filled-basic" label="Apellido" variant="filled" />
        <TextField id="outlined-basic" label="Diereccion" variant="outlined" />
        <Button variant="contained" color="primary" onClick={guardarDatos}>
          Guardar datos
        </Button>
        <Button variant="contained" color="primary" onClick={getDatos}>
          Mostrar datos
        </Button>
      </form>
      <Button variant="contained" color="primary" onClick={handleLoading}>
        Ver loading
      </Button>
      <Button variant="contained" color="primary" onClick={goToPage}>
        ir a result
      </Button>
    </Fragment>
  );
};

const mapStateToProps = state => {
  return {
    loading: state.loading,
    formulario: state.formulario
  };
};

const mapDispatchToProps = {
  showLoading,
  hideLoading,
  setDatosForm,
  getDatosForm
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
