export const type = "getDatosForm";

const getDatosForm = estado => {
  return {
    type,
    payload: estado
  };
};

export default getDatosForm;
