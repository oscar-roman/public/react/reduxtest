export const type = "setDatosForm";

const setDatosForm = estado => {
  return {
    type,
    payload: estado
  };
};

export default setDatosForm;
