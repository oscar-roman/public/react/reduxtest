export const type = "hideLoading";

const hideLoading = estado => {
  return {
    type,
    payload: estado
  };
};

export default hideLoading;
