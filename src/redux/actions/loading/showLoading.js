export const type = "showLoading";

const showLoading = estado => {
  return {
    type,
    payload: estado
  };
};

export default showLoading;
