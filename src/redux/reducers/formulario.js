import { type as getDatosForm } from "../actions/formulario/getDatosForm";
import { type as setDatosForm } from "../actions/formulario/setDatosForm";

const defaultState = {};

const reducer = (state = defaultState, { type, payload }) => {
  switch (type) {
    case getDatosForm: {
      return state;
    }
    case setDatosForm: {
      state = payload;
      return state;
    }
    default:
      return state;
  }
};

export default reducer;
