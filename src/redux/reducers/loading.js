import { type as showLoading } from "../actions/loading/showLoading";
import { type as hideLoading } from "../actions/loading/hideLoading";

const defaultState = false;

const reducer = (state = defaultState, { type }) => {
  switch (type) {
    case showLoading: {
      state = true;
      return state;
    }
    case hideLoading: {
      state = false;
      return state;
    }
    default:
      return state;
  }
};

export default reducer;
