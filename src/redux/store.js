import { createStore, combineReducers } from "redux";
import results from "./reducers/results";
import loading from "./reducers/loading";
import formulario from './reducers/formulario';

const reducer = combineReducers({
  results,
  loading,
  formulario
});

const store = createStore(reducer);

export default store;
