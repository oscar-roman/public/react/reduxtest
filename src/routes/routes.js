import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import Results from '../components/results';
import Details from '../components/details';
import ResultForm from '../components/resultForm';

const Routes = ()=>{
  return (
  <BrowserRouter>
    <Switch>
      <Route path="/results" component={Results} />
      <Route path="/resultsForm" component={ResultForm} />
      <Route path="/details/:itenId" component={Details} />
      <Redirect from="/" to="/results" />
    </Switch>
  </BrowserRouter>
  );
}

export default Routes;
